LCL-CodeChallenge
=======

The purpose of this project is to demonstrate question 1's answer.
Moreover, the unit testing part of this project can demo how to unit test blocks used asynchronously (Question 2). 

Downloading the Code
----------------

You'll need a few things before we get started. Make sure you have Xcode installed from 
the App Store or wherever. Then run the following two commands to install Xcode's
command line tools and `bundler`, if you don't have that yet.

```sh
[sudo] gem install bundler
xcode-select --install
```

Then run this command to build project.

```sh
make app
```

Getting Started
---------------

Now that we have the code [downloaded](#downloading-the-code), we can run the
app using [Xcode 7](https://developer.apple.com/xcode/download/). Make sure to
open the `LCL-CodeChallenge.xcworkspace` workspace, and not the `LCL-CodeChallenge.xcodeproj` project.
Currently, the project is compatible with Xcode 7 only, as it's Swift 2.
