WORKSPACE = LCL-CodeChallenge.xcworkspace
SCHEME = LCL-CodeChallenge
CONFIGURATION = Beta
APP_PLIST = LCL-CodeChallenge/Resources/Other Resources/Info.plist
PLIST_BUDDY = /usr/libexec/PlistBuddy
DEVICE_HOST = platform='iOS Simulator',OS='9.0',name='iPhone 6'

GIT_COMMIT_REV = $(shell git log -n1 --format='%h')
GIT_COMMIT_SHA = $(shell git log -n1 --format='%H')
GIT_REMOTE_ORIGIN_URL = $(shell git config --get remote.origin.url)

DATE_MONTH = $(shell date "+%e %h" | tr "[:lower:]" "[:upper:]")
DATE_VERSION = $(shell date "+%Y.%m.%d")

LOCAL_BRANCH = $(shell git rev-parse --abbrev-ref HEAD)
BRANCH = $(shell echo $(shell whoami)-$(shell git rev-parse --abbrev-ref HEAD))

all: ci

### General setup

bundler:
	gem install bundler
	bundle install

app:
	bundle exec pod install
	open LCL-CodeChallenge.xcworkspace
