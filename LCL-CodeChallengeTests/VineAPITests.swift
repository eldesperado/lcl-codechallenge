//
//  VineAPITests.swift
//  LCL-CodeChallenge
//
//  Created by Pham Nguyen Nhat Trung on 3/2/16.
//  Copyright © 2016 Pham Nguyen Nhat Trung. All rights reserved.
//

import XCTest
import RxSwift

@testable import LCL_CodeChallenge

class VineAPITests: XCTestCase {
    var disposeBag: DisposeBag!
    
    override func setUp() {
        super.setUp()
        disposeBag = DisposeBag()
    }
    
    override func tearDown() {
        // Put teardown code here. This method is called after the invocation of each test method in the class.
        super.tearDown()
    }

    func testExample() {
        // This is an example of a functional test case.
        // Use XCTAssert and related functions to verify your tests produce the correct results.
    }

    func testAsynchronousAPIRequest() {
        let asyncExpectation = expectationWithDescription("longRunningFunction")
        
        let endpoint = VineAPIEndpoint.Login(username: "hoangtusuhoaqua@mailnesia.com", password: "747548")
        
        var testUser: User?
        
        _ = VineAPI
            .sendRequest(endpoint)
            .mapToObject(User) // Mapping JSON to User
            .subscribeNext({ user in
                // Show welcome message if signed-in successfully
                testUser = user
                // Fullfill expectation
                asyncExpectation.fulfill()
            }).addDisposableTo(disposeBag)
        
        waitForExpectationsWithTimeout(60) { error in
            XCTAssertNotNil(testUser, "Can not sign-in")
            XCTAssertNotNil(testUser?.userName, "Can not get user data")
        }
    }

}
