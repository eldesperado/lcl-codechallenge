//
//  HelperFunctions.swift
//  LCL-CodeChallenge
//
//  Created by Pham Nguyen Nhat Trung on 3/2/16.
//  Copyright © 2016 Pham Nguyen Nhat Trung. All rights reserved.
//

import RxSwift
import UIKit

// Collection of stanardised mapping funtions for Rx work
func void<T>(_: T) -> Void {
    return Void()
}
