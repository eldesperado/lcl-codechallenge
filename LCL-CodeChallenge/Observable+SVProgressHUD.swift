//
//  Observable+SVProgressHUD.swift
//  LCL-CodeChallenge
//
//  Created by Pham Nguyen Nhat Trung on 3/2/16.
//  Copyright © 2016 Pham Nguyen Nhat Trung. All rights reserved.
//

import RxSwift
import SVProgressHUD

extension Observable {
    func showMessageIfErrorOccurred(prefix: String = "Error: ") -> Observable<Element> {
        return self.doOn { event in
            switch event {
            case .Error(let error):
                doOnMainThread({ 
                    SVProgressHUD.showErrorWithStatus("Sign-in failed!")
                    print("\(prefix)\(error)")
                })
            default: break
            }
        }
    }
    
    func showMessageIfSucceed(message: String) -> Observable<Element> {
        return self.doOn { event in
            switch event {
            case .Next:
                doOnMainThread({
                    SVProgressHUD.showSuccessWithStatus(message)
                })
            default: break
            }
        }
    }
}
