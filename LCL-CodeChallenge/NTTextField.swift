//
//  NTTextField.swift
//  LCL-CodeChallenge
//
//  Created by Pham Nguyen Nhat Trung on 3/2/16.
//  Copyright © 2016 Pham Nguyen Nhat Trung. All rights reserved.
//

import UIKit

public class NTTextField: UITextField {

    let padding = UIEdgeInsets(top: 0, left: 20, bottom: 0, right: 20);
    
    override public func textRectForBounds(bounds: CGRect) -> CGRect {
        return self.newBounds(bounds)
    }
    
    override public func placeholderRectForBounds(bounds: CGRect) -> CGRect {
        return self.newBounds(bounds)
    }
    
    override public func editingRectForBounds(bounds: CGRect) -> CGRect {
        return self.newBounds(bounds)
    }
    
    private func newBounds(bounds: CGRect) -> CGRect {
        
        var newBounds = bounds
        newBounds.origin.x += padding.left
        newBounds.origin.y += padding.top
        newBounds.size.height -= padding.top + padding.bottom
        newBounds.size.width -= padding.left + padding.right
        return newBounds
    }

    // MARK: Initialization
    required public init?(coder aDecoder: NSCoder) {
        super.init(coder: aDecoder)
        // Setup Layouts
        setupLayouts()
    }
    
    // MARK: Private Methods
    private func setupLayouts() {
        cornerRadius = 10
        borderWidth = 1
        let placeHolderString = placeholder ?? ""
        let placeHolderColor = UIColor.defaultPlaceholderTextFieldColor()
        attributedPlaceholder = NSAttributedString(string: placeHolderString,
            attributes:[NSForegroundColorAttributeName: placeHolderColor])
    }
}
