//
//  Observable+Operators.swift
//  LCL-CodeChallenge
//
//  Created by Pham Nguyen Nhat Trung on 3/2/16.
//  Copyright © 2016 Pham Nguyen Nhat Trung. All rights reserved.
//

import RxSwift

extension Observable {
    func doOnNext(closure: Element -> Void) -> Observable<Element> {
        return doOn { (event: Event) in
            switch event {
            case .Next(let value):
                closure(value)
            default: break
            }
        }
    }
    
    func doOnCompleted(closure: () -> Void) -> Observable<Element> {
        return doOn { (event: Event) in
            switch event {
            case .Completed:
                closure()
            default: break
            }
        }
    }
    
    func doOnError(closure: ErrorType -> Void) -> Observable<Element> {
        return doOn { (event: Event) in
            switch event {
            case .Error(let error):
                closure(error)
            default: break
            }
        }
    }
}

// Maps true to false and vice versa
extension Observable where Element: BooleanType {
    func not() -> Observable<Bool> {
        return self.map { input in
            return !input.boolValue
        }
    }
}

extension ObservableType {
    
    func then(closure: () -> Observable<E>?) -> Observable<E> {
        return then(closure() ?? .empty())
    }
    
    func then(@autoclosure(escaping) closure: () -> Observable<E>) -> Observable<E> {
        let next = Observable.deferred {
            return closure() ?? .empty()
        }
        
        return self
            .ignoreElements()
            .concat(next)
    }
}