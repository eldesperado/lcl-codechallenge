//
//  LoginViewController.swift
//  LCL-CodeChallenge
//
//  Created by Pham Nguyen Nhat Trung on 3/2/16.
//  Copyright © 2016 Pham Nguyen Nhat Trung. All rights reserved.
//

import UIKit
import RxSwift
import NSObject_Rx
import Action
import SVProgressHUD

class LoginViewController: UIViewController {
    // MARK: IBOutlets
    @IBOutlet weak var userNameTextField: UITextField!
    @IBOutlet weak var passwordTextField: UITextField!
    @IBOutlet weak var signInButton: UIButton!
    
    // MARK: Private Attributes
    private lazy var validator: Validator = Validator()
    
    // MARK: Life Cycles
    override func viewDidLoad() {
        super.viewDidLoad()
        // Setup
        setup()
    }
    
    // MARK: Setup
    private func setup() {
        // Does username field's input valid for email format
        // and password field's input is not empty
        //   if so enable sign-in button
        //   else disable sign-in button
        
        // Get user name & password
        let username = userNameTextField.rx_text
        let password = passwordTextField.rx_text
        
        // Create username validation
        let usernameValidation = username
            .map { [weak self] username -> Observable<Validator.ValidationResult> in
                guard let strongSelf = self else { return .empty() }
                return strongSelf.validator.validateUsername(username)
            }
            .switchLatest() // Always switch to the latest event occurred
            .shareReplay(1) // Keep only 1 allocation
        
        // Create password validation
        let passwordValidation = password
            .map { [weak self] password -> Observable<Validator.ValidationResult> in
                guard let strongSelf = self else { return .empty() }
                return strongSelf.validator.validatePassword(password)
            }
            .switchLatest()
            .shareReplay(1)
        
        // Create input validation
        let isValidInput = Observable
            .combineLatest(usernameValidation, passwordValidation) { (usrVal, pwdVal) -> Bool in
                guard let isUsernameValid = usrVal.valid, isPasswordValid = pwdVal.valid else { return false }
                return isUsernameValid && isPasswordValid
            }

        // Create action for sign-in button
        signInButton.rx_action = CocoaAction(enabledIf: isValidInput) { [weak self] _ in
            guard let strongSelf = self,
                usernameString = strongSelf.userNameTextField.text,
                passwordString = strongSelf.passwordTextField.text else { return .empty() }

            let endpoint = VineAPIEndpoint.Login(username: usernameString, password: passwordString)
            
            // Show HUD
            SVProgressHUD.show()
            
            // Send sign-in request, if successfully then shows a message with
            // this following format "Howdy, <User Name>!"
            // Otherwise, shows an error message
            return VineAPI.sendRequest(endpoint) // Send API request to get response
                .mapToObject(User) // Then maps returned response to JSON, then to User
                .doOnNext({ user in
                    // Show welcome message if signed-in successfully
                    let username = user.userName
                    // Show
                    doOnMainThread({
                        SVProgressHUD.showSuccessWithStatus("Howdy, \(username)!")
                    })
                })
                .showMessageIfErrorOccurred() // Show error message if occurred
                .map(void)
        }
        
        // Bind Validation Result to UI
        // Bind username validation result to username text field
        bindValidationResultToUI(usernameValidation, validationTextField: userNameTextField)
        // Bind password validation result to password text field
        bindValidationResultToUI(passwordValidation, validationTextField: passwordTextField)
        
    }
    
    // MARK: Private Methods
    // MARK: Binding Error to UI
    private func bindValidationResultToUI(source: Observable<Validator.ValidationResult>,
         validationTextField: UITextField) {
        source
            .skip(1)
            .subscribeNext { result in
                let validationColor: UIColor
                
                if let valid = result.valid {
                    validationColor = valid ? UIColor.greenColor() : UIColor.redColor()
                }
                else {
                    validationColor = UIColor.clearColor()
                }
                
                validationTextField.borderColor = validationColor
            }
            .addDisposableTo(rx_disposeBag)
    }
}
