//
//  VineAPI.swift
//  LCL-CodeChallenge
//
//  Created by Pham Nguyen Nhat Trung on 3/2/16.
//  Copyright © 2016 Pham Nguyen Nhat Trung. All rights reserved.
//

import Foundation
import RxSwift
import RxCocoa

// MARK: Protocols
protocol Endpoint {
    var baseURL: NSURL { get }
    var path : String { get }
    var method: Method { get }
    var parameters: [String: AnyObject]? { get }
    var parameterEncoding: ParameterEncoding { get }
    var httpHeaderFields: [String: String]? { get }
    var urlRequest: NSURLRequest? { get }
}

// MARK: Enums
enum Method: String {
    case GET, POST
}

enum VineAPIEndpoint {
    case Login(username: String, password: String)
}

extension VineAPIEndpoint: Endpoint {
    var baseURL: NSURL { return NSURL(string: "https://api.vineapp.com/")! }
    var path: String {
        switch self {
        case .Login:
            return "users/authenticate"
        }
    }
    var method: Method {
        switch self {
        case .Login:
            return .POST
        }
    }
    var parameters: [String : AnyObject]? {
        switch self {
        case .Login(let username, let password):
            return ["username": username, "password": password]
        }
    }
    
    var parameterEncoding: ParameterEncoding {
        return ParameterEncoding.JSON
    }
    
    var urlRequest: NSURLRequest? {
        let url = baseURL.URLByAppendingPathComponent(path).absoluteString
        let request = NSMutableURLRequest(URL: NSURL(string: url)!)
        request.HTTPMethod = method.rawValue
        request.allHTTPHeaderFields = httpHeaderFields
        
        return parameterEncoding.encode(request, parameters: parameters).0
    }
    
    var httpHeaderFields: [String : String]? {
        return [:]
    }
}

final class VineAPI: NSObject {
    static func sendRequest(endpoint: VineAPIEndpoint) -> Observable<AnyObject> {
        guard let urlRequest = endpoint.urlRequest else { return .empty() }
        return NSURLSession.sharedSession().rx_JSON(urlRequest)
    }
}