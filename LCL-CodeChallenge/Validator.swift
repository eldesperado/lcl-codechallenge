//
//  Validator.swift
//  LCL-CodeChallenge
//
//  Created by Pham Nguyen Nhat Trung on 3/2/16.
//  Copyright © 2016 Pham Nguyen Nhat Trung. All rights reserved.
//

import RxSwift

struct Validator {
    typealias ValidationResult = (valid: Bool?, message: String?)
    
    func validateUsername(username: String) -> Observable<ValidationResult> {
        // Check whether given input is empty or not
        // if empty, return false, otherwise continue
        if isZeroLengthString(username) {
            return .just((false, "Username can not be empty"))
        }
        // Check whether given input is in email format or not
        // if not return false, otherwise continue
        if !stringIsEmailAddress(username) {
            return .just((false, "Username must be in email format"))
        }
        // Pass validation, return true
        return .just((true, "Valid Username"))
    }
    
    func validatePassword(password: String) -> Observable<ValidationResult> {
        // Check whether given input is empty or not
        // if empty, return false, otherwise continue
        if isZeroLengthString(password) {
            return .just((false, "Password can not be empty"))
        }
        
        // Pass validation
        return .just((true, "Password acceptable"))
    }

}

private extension Validator {
    func stringIsEmailAddress(text: String) -> Bool {
        let emailRegex = "[A-Z0-9a-z._%+-]+@[A-Za-z0-9.-]+\\.[A-Za-z]{2,6}"
        let testPredicate = NSPredicate(format:"SELF MATCHES %@", emailRegex)
        return testPredicate.evaluateWithObject(text)
    }
    
    
    func isZeroLengthString(string: String) -> Bool {
        return string.isEmpty
    }
}