//
//  User.swift
//  LCL-CodeChallenge
//
//  Created by Pham Nguyen Nhat Trung on 3/2/16.
//  Copyright © 2016 Pham Nguyen Nhat Trung. All rights reserved.
//

import Foundation
import SwiftyJSON

protocol JSONAbleType {
    static func fromJSON(_: [String: AnyObject]) -> Self
}

final class User: NSObject {
    dynamic let userId: String
    dynamic let userName: String
    
    init(userId: String, userName: String) {
        self.userId = userId
        self.userName = userName
    }
}

extension User: JSONAbleType {
    static func fromJSON(json:[String: AnyObject]) -> User {
        let json = JSON(json)["data"]
        
        let userId = json["userId"].stringValue
        let userName = json["username"].stringValue
        return User(userId: userId, userName: userName)
    }
}