//
//  UIColor+App.swift
//  LCL-CodeChallenge
//
//  Created by Pham Nguyen Nhat Trung on 3/2/16.
//  Copyright © 2016 Pham Nguyen Nhat Trung. All rights reserved.
//

import UIKit

extension UIColor {
    static func defaultTextFieldBGColor() -> UIColor {
        return UIColor(red: 58.0 / 255.0, green: 73.0 / 255.0, blue: 84.0 / 255.0, alpha: 1)
    }
    
    static func defaultPlaceholderTextFieldColor() -> UIColor {
        return UIColor(red: 99.0 / 255.0, green: 117.0 / 255.0, blue: 130.0 / 255.0, alpha: 1)
    }
}
