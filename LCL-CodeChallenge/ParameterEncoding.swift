//
//  ParameterEncoding.swift
//  LCL-CodeChallenge
//
//  Created by Pham Nguyen Nhat Trung on 3/2/16.
//  Copyright © 2016 Pham Nguyen Nhat Trung. All rights reserved.
//

import Foundation

enum ParameterEncoding {
    case JSON
    
    func encode(URLRequest: NSURLRequest, parameters: [String: AnyObject]?)
        -> (NSMutableURLRequest?, NSError?) {
            guard let mutableURLRequest = URLRequest.mutableCopy() as? NSMutableURLRequest else { return (nil, nil) }
            guard let parameters = parameters else { return (mutableURLRequest, nil) }
            
            var encodingError: NSError? = nil
            
            switch self {
            case .JSON:
                do {
                    let options = NSJSONWritingOptions()
                    let data = try NSJSONSerialization.dataWithJSONObject(parameters, options: options)
                    
                    if mutableURLRequest.valueForHTTPHeaderField("Content-Type") == nil {
                        mutableURLRequest.setValue("application/json", forHTTPHeaderField: "Content-Type")
                    }
                    
                    mutableURLRequest.HTTPBody = data
                } catch {
                    encodingError = error as NSError
                }
            }
            return (mutableURLRequest, encodingError)
    }
}