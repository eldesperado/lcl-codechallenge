//
//  GlobalFunctions.swift
//  LCL-CodeChallenge
//
//  Created by Pham Nguyen Nhat Trung on 3/2/16.
//  Copyright © 2016 Pham Nguyen Nhat Trung. All rights reserved.
//

import Foundation

func doOnMainThread(closure: () -> ()) {
    dispatch_async(dispatch_get_main_queue()) { 
        closure()
    }
}