//
//  Observable+JSONAbleType.swift
//  LCL-CodeChallenge
//
//  Created by Pham Nguyen Nhat Trung on 3/2/16.
//  Copyright © 2016 Pham Nguyen Nhat Trung. All rights reserved.
//

import Foundation
import RxSwift

enum JSONableError: String, ErrorType {
    case CouldNotParseJSON
}

extension Observable {
    typealias Dictionary = [String: AnyObject]
    
    /// Get given JSONified data, pass back objects
    func mapToObject<B: JSONAbleType>(classType: B.Type) -> Observable<B> {
        return self.map { json in
            guard let dict = json as? Dictionary else {
                throw JSONableError.CouldNotParseJSON
            }
            
            return B.fromJSON(dict)
        }
    }
    
    /// Get given JSONified data, pass back objects as an array
    func mapToObjectArray<B: JSONAbleType>(classType: B.Type) -> Observable<[B]> {
        return self.map { json in
            guard let array = json as? [AnyObject] else {
                throw JSONableError.CouldNotParseJSON
            }
            
            guard let dicts = array as? [Dictionary] else {
                throw JSONableError.CouldNotParseJSON
            }
            
            return dicts.map { B.fromJSON($0) }
        }
    }
}